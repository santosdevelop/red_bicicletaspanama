var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId) {
    for (var i=0; i<Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'rojo', 'urbana', [9.0496006,-79.5041744]);
var b = new Bicicleta(2, 'blanca', 'urbana', [9.0341393,-79.5250323]);
var c = new Bicicleta(3, 'morado', 'urbana', [8.8743349,-79.8464046]); //Chorrera
var d = new Bicicleta(4, 'azul', 'urbana', [9.0584609,-79.5643756]); //San Miguelito
var e = new Bicicleta(5, 'verde', 'urbana', [9.0769668,-79.5084538]); //Belisario Frias
var f = new Bicicleta(6, 'amarillo', 'urbana', [8.9755481,-79.5182008]); //Punta Paitilla

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
Bicicleta.add(e);
Bicicleta.add(f);

module.exports = Bicicleta;
