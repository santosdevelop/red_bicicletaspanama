var map = L.map('main_map').setView([9.0813885, -79.5932239], 11);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 10
}).addTo(map);

//L.marker([8.8743349,-79.8464046]).addTo(map); //Chorrera
//L.marker([9.0584609,-79.5643756]).addTo(map); //San Miguelito
//L.marker([9.0769668,-79.5084538]).addTo(map); //Belisario Frias
//L.marker([8.9755481,-79.5182008]).addTo(map); //Punta Paitilla

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach( function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})
